import Axios from 'axios';

const auth_username = 'dashboard',
      auth_password = 'frontendsj';

export default class DataService {
  static get_instance() {
    if (!DataService._instance) {
      DataService._instance = new DataService();
    }
    return DataService._instance;
  }

  get_authentication_token () {
    return {
      auth: {
        username: auth_username,
        password: auth_password
      }
    }
  }

  fetch_productivity() {
    return Axios.get('http://dashboard.hyakuren.org/fe/api/v1/productivity_chart_data.json', this.get_authentication_token())
  }

  fetch_gitlab_merge_requests() {
    return Axios.get('http://dashboard.hyakuren.org/fe/api/v1/gitlab.json', this.get_authentication_token())
  }

  fetch_statistics() {
    return Axios.get('http://dashboard.hyakuren.org/fe/api/v1/jira_members.json', this.get_authentication_token())
  }
}
