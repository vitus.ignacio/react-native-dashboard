/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {
  Component,
} from 'react';
import {
  StyleSheet,
  View
} from 'react-native';

import BaseNavigator from './components/BaseNavigator'

export default class App extends Component {
  render() {
    return (
      <View style={{
          flex: 1,
          backgroundColor: '#fff'
      }}>
          <BaseNavigator/>
      </View>
    );
  }
}
