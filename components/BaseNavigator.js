import React from 'react';
import Icon from 'react-native-fa-icons';
import {createBottomTabNavigator} from 'react-navigation';

import About from '../views/About';
import Overview from '../views/Overview';
import MergeRequest from '../views/MergeRequest';
import Statistics from '../views/Statistics';

const TABBAR_ICON_FOREGROUND = '#FFFFFF';

export default BaseNavigator = createBottomTabNavigator({
    Overview: {
        screen: Overview,
        navigationOptions: () => ({
            tabBarIcon: ({tintColor}) => (
                <Icon name='area-chart' style={{ fontSize: 24, color: TABBAR_ICON_FOREGROUND }} />
            )
        })
    },
    MergeRequests: {
        screen: MergeRequest,
        navigationOptions: () => ({
            tabBarIcon: ({tintColor}) => (
                <Icon name='tasks' style={{ fontSize: 24, color: TABBAR_ICON_FOREGROUND }} />
            )
        })
    },
    Statistics: {
        screen: Statistics,
        navigationOptions: () => ({
            tabBarIcon: ({tintColor}) => (
                <Icon name='address-card' style={{ fontSize: 24, color: TABBAR_ICON_FOREGROUND }} />
            )
        })
    },
    News: {
        screen: About,
        navigationOptions: () => ({
            tabBarIcon: ({tintColor}) => (
                <Icon name='bookmark' style={{ fontSize: 24, color: TABBAR_ICON_FOREGROUND }} />
            )
        })
    }
}, {
    tabBarOptions: {
        showLabel: false,
        activeTintColor: '#F8F8F8',
        inactiveTintColor: '#586589',
        style: {
            backgroundColor: '#B22222'
        },
        tabStyle: {}
    }
});