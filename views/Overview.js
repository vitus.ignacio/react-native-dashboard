import _ from 'underscore';
import uuidv1 from 'uuid/v1'; 

import React, {
  Component
} from 'react';
import { Dimensions, Platform, StyleSheet, Text, View, ScrollView } from 'react-native';
import LineChart from '../libraries/react-native-chart-kit/line-chart';

import DataService from '../api/DataService';

const window_width = Dimensions.get('window').width,
      window_height = Dimensions.get('window').height;

export default class Overview extends Component {
  constructor(props) {
    super(props);

    this.state = {
      productivity: [
        {
          color: 'rgba(255, 255, 255, 0)',
          name: '',
          data: []
        }
      ],
      productivity_labels: []
    }

    this.fetch_productivity();

    this.fetch_data();
  }

  fetch_data() {
    let _self = this;
    setInterval(() => {
      _self.fetch_productivity()
    }, 600000)
  }

  fetch_productivity() {
    let _self = this;
    DataService.get_instance().fetch_productivity()
      .then((res) => {
        if (res.data) {
          let members = []
          res.data.map((v) => {
            let member_data =
            {
              color: `rgba(${(Math.random()*255).toFixed(0)}, ${(Math.random()*255).toFixed(0)}, ${(Math.random()*255).toFixed(0)}, 0.7)`,
              name: v.friendly_id,
              data: []
            }
            if (v.productivity_data && v.productivity_data.length > 0) {
              v.productivity_data.map((p, p_index) => {
                if (p_index <= 1000 && p_index % 200 === 0) {
                  member_data.data.push(parseFloat(((p/100) * 9).toFixed(2)));
                } else if (p_index > 1000 && p_index <= 5000 && p_index % 500 === 0) {
                  member_data.data.push(parseFloat(((p/100) * 9).toFixed(2)));
                } else if (p_index > 5000 && p_index <= 10000 && p_index % 1000 === 0) {
                  member_data.data.push(parseFloat(((p/100) * 9).toFixed(2)));
                } else if (p_index > 10000 && p_index % 2000 === 0) {
                  member_data.data.push(parseFloat(((p/100) * 9).toFixed(2)));
                }
              })
              member_data.data.push(parseFloat(((v.productivity_data[v.productivity_data.length - 1]/100) * 9).toFixed(2)))
            }
            members.push(member_data)
          });
          _self.setState({
            productivity: _.sortBy(members, (m) => m.name)
          })
        }
      })
      .catch((err) => {
        return;
      })
  }

  render() {
    let productivity_legend = [],
        productivity_charts = [];
    this.state.productivity.map((item, index) => {
      productivity_legend.push(
        <View key={uuidv1()} style={{ flexDirection: 'row', marginRight: 5, marginTop: 5, marginBottom: 5 }}>
          <View style={{
            width: 30,
            height: 18,
            backgroundColor: item.color
          }} /><Text style={{ color: item.color, marginLeft: 5 }}>{item.name}</Text>
        </View>
      )
      productivity_charts.push(
        <LineChart key={uuidv1()}
          data={{
            labels: this.state.productivity_labels,
            datasets: [this.state.productivity[index] || {}]
          }}
          width={window_width - 40} // from react-native
          height={180}
          chartConfig={{
            backgroundGradientFrom: '#fff',
            backgroundGradientTo: '#fff',
            color: (opacity = 1) => `rgba(0, 0, 0, 0.7)`,
            dotColor: () => `rgba(0, 0, 0, 0.7)`,
            labelColor: () => `rgba(0, 0, 0, 0.7)`,
            style: {
              borderRadius: 0
            }
          }}
          bezier
          style={{
            borderRadius: 0,
            marginVertical: 8
          }}
        />
      )
    })

    return (
      <View style={{
          alignItems: 'center',
          backgroundColor: 'transparent',
          flex: 1,
          flexDirection: 'column',
          justifyContent: 'flex-start',
          marginTop: 30
        }}>
        <ScrollView style={{ width: window_width, paddingTop: 40, paddingLeft: 20, paddingRight: 20 }}>
          <Text style={{
            fontSize: 40, 
            fontWeight: 'bold',
            marginBottom: 28 
          }}>Productivity</Text>
          <View style={{
            flexDirection: 'row',
            flexWrap: 'wrap'
          }}>
            {productivity_legend}
          </View>
          {productivity_charts}
          <View style={{ height: 40 }}></View>
        </ScrollView>
      </View>
    );
  }
}
