import _ from 'underscore';
import uuidv1 from 'uuid/v1'; 

import React, {
  Component
} from 'react';
import { Dimensions, Platform, StyleSheet, Image, Text, View, ScrollView, Linking } from 'react-native';
import { Avatar, Card, ListItem, Button, Icon, Badge } from '../libraries/react-native-elements';

import DataService from '../api/DataService';

const window_width = Dimensions.get('window').width,
      window_height = Dimensions.get('window').height;

export default class MergeRequest extends Component{
  constructor(props) {
    super(props);

    this.state = {
      merge_requests: []
    }

    this.fetch_gitlab_merge_requests();

    this.fetch_data();
  }

  fetch_data() {
    let _self = this;
    setInterval(() => {
      _self.fetch_gitlab_merge_requests()
    }, 120000)
  }

  fetch_gitlab_merge_requests() {
    let _self = this;
    DataService.get_instance().fetch_gitlab_merge_requests()
      .then((res) => {
        if (res.data) {
          let merge_requests = [];
          res.data.open_merge_requests_with_frontend_label.map((v) => {
            let item = {
              title: v.title,
              author: v.author,
              assignee: v.assignee,
              labels: [],
              url: v.web_url
            }, item_labels = [];
            item_labels.push({
              backgroundColor: '#f1ab00',
              value: v.milestone || '♨️',
            })
            v.labels.map((l, j) => {
              item_labels.push({
                backgroundColor: '#cd1e10',
                value: l
              })
            })
            item.labels = item_labels;
            merge_requests.push(item)
          })
          _self.setState({
            merge_requests: _.sortBy(merge_requests, (m) => m.title)
          })
        }
      })
      .catch((err) => {
        return;
      })
  }

  render() {
    let _self = this;

    let merge_request_elements = []
    this.state.merge_requests.map((m, i) => {
      let merge_request_labels_elements = []
      m.labels.map((l, j) => {
        merge_request_labels_elements.push(
          <Badge key={uuidv1()}
            value={l.value}
            textStyle={{ color: '#FFF' }}
            wrapperStyle={{ flexDirection: 'row', justifyContent: 'flex-start', margin: 4 }}
            containerStyle={{ backgroundColor: l.backgroundColor }}
          />
        )
      })
      merge_request_elements.push(
        <Card key={uuidv1()}
          title={m.title}
          titleStyle={{
            textAlign: 'left'
          }}>
          { 
            <View>
              <Text style={{
                marginBottom: 6
              }}>{`Author: ${m.author}`}</Text>
              <Text>{`Assignee: ${m.assignee}`}</Text>
              <View style={{
                flexDirection: 'row',
                flexWrap: 'wrap',
                marginTop: 8,
                marginLeft: -4
              }}>
                {merge_request_labels_elements}
              </View>
            </View>
          }
          <Button
            buttonStyle={{ backgroundColor: '#B22222', borderRadius: 0, marginLeft: 0, marginRight: 0, marginBottom: 0 }}
            style={{
              marginTop: 10
            }}
            title='Show More'
            titleStyle={{ fontSize: 14 }}
            onPress={() => {
              Linking.openURL(m.url)
            }} />
        </Card>
      )
    })

    return (
      <View style={{
          alignItems: 'center',
          backgroundColor: 'transparent',
          flex: 1,
          flexDirection: 'column',
          justifyContent: 'flex-start',
          marginTop: 30
        }}>
        {this.state.merge_requests.length > 0 ?
          <ScrollView style={{ width: window_width, paddingTop: 40, paddingLeft: 10, paddingRight: 10 }}>
            {merge_request_elements}
            <View style={{ height: 80 }}></View>
          </ScrollView> :
          <View style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: 'transparent',
          }}>
            <Avatar
              avatarStyle={{
                backgroundColor: 'transparent'
              }}
              size={'xlarge'}
              rounded
              source={{uri: 'https://images-na.ssl-images-amazon.com/images/I/61HhvJJGBIL._SX425_.jpg'}}
              activeOpacity={0.7}
            />
            <Text style={{
              fontSize: 20,
              textAlign: 'center',
              margin: 10,
            }}>LMAO... Nothing here!</Text>
          </View>
        }
      </View>
    );
  }
}
