import _ from 'underscore';
import uuidv1 from 'uuid/v1'; 

import React, {
  Component
} from 'react';
import { Dimensions, Platform, StyleSheet, Text, View, ScrollView } from 'react-native';
import { Avatar, Card, ListItem, Button, Icon, Badge } from '../libraries/react-native-elements';

import DataService from '../api/DataService';

const window_width = Dimensions.get('window').width,
      window_height = Dimensions.get('window').height;

export default class Statistics extends Component {
  constructor(props) {
    super(props);

    this.state = {
      statistics: []
    }

    this.fetch_statistics();

    this.fetch_data();
  }

  fetch_data() {
    let _self = this;
    setInterval(() => {
      _self.fetch_statistics()
    }, 600000)
  }

  fetch_statistics() {
    let _self = this;
    DataService.get_instance().fetch_statistics()
      .then((res) => {
        if (res.data) {
          _self.setState({
            statistics: _.sortBy(res.data, (d) => d.name)
          })
        }
      })
      .catch((err) => {
        return
      })
  }
  
  render() {
    return (
      <View style={{
          alignItems: 'center',
          backgroundColor: 'transparent',
          flex: 1,
          flexDirection: 'column',
          justifyContent: 'flex-start',
          marginTop: 30
        }}>
      { this.state.statistics.length > 0 ?
        <ScrollView style={{ width: window_width, paddingTop: 40, paddingLeft: 10, paddingRight: 10 }}>
          <View>
            {
              this.state.statistics.map((s, index) => {
                return (
                  <Card key={uuidv1()}
                    title={s.name} 
                    containerStyle={{padding: 15}}
                    titleStyle={{
                      textAlign: 'left'
                    }}>
                    <View
                      style={{
                        flex: 1,
                        flexDirection: 'row',
                        justifyContent: 'space-between'
                      }}>
                      <Text
                        style={{
                          paddingTop: 5,
                          paddingBottom: 5,
                          fontWeight: 'bold'
                        }}>Story Points</Text>
                      <Text
                        style={{
                          paddingTop: 5,
                          paddingBottom: 5
                        }}>{s.sps}</Text>
                    </View>
                    <View
                      style={{
                        flex: 1,
                        flexDirection: 'row',
                        justifyContent: 'space-between'
                      }}>
                      <Text
                        style={{
                          paddingTop: 5,
                          paddingBottom: 5,
                          fontWeight: 'bold'
                        }}>Tickets</Text>
                      <Text
                        style={{
                          paddingTop: 5,
                          paddingBottom: 5
                        }}>{s.tickets}</Text>
                    </View>
                    <View
                      style={{
                        flex: 1,
                        flexDirection: 'row',
                        justifyContent: 'space-between'
                      }}>
                      <Text
                        style={{
                          paddingTop: 5,
                          paddingBottom: 5,
                          fontWeight: 'bold'
                        }}>Worklog</Text>
                      <Text
                        style={{
                          paddingTop: 5,
                          paddingBottom: 5
                        }}>{s.worklogs}</Text>
                    </View>
                    <View
                      style={{
                        flex: 1,
                        flexDirection: 'row',
                        justifyContent: 'space-between'
                      }}>
                      <Text
                        style={{
                          paddingTop: 5,
                          paddingBottom: 5,
                          fontWeight: 'bold'
                        }}>Working Hours/Ticket</Text>
                      <Text
                        style={{
                          paddingTop: 5,
                          paddingBottom: 5
                        }}>{((s.worklogs / s.tickets) * 9).toFixed(2)}</Text>
                    </View>
                    <View
                      style={{
                        flex: 1,
                        flexDirection: 'row',
                        justifyContent: 'space-between'
                      }}>
                      <Text
                        style={{
                          paddingTop: 5,
                          paddingBottom: 5,
                          fontWeight: 'bold'
                        }}>SPs Earn Rate</Text>
                      <Text
                        style={{
                          paddingTop: 5,
                          paddingBottom: 5
                        }}>{(s.sps / s.tickets).toFixed(2)}</Text>
                    </View>
                  </Card>
                )
              })
            }
          </View>
          <View style={{ height: 80 }}></View>
        </ScrollView>
        :
        <View style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: 'transparent',
        }}>
          <Avatar
            avatarStyle={{
              backgroundColor: 'transparent'
            }}
            size={'xlarge'}
            rounded
            source={{uri: 'https://images-na.ssl-images-amazon.com/images/I/61HhvJJGBIL._SX425_.jpg'}}
            activeOpacity={0.7}
          />
          <Text style={{
            fontSize: 20,
            textAlign: 'center',
            margin: 10,
          }}>LMAO... Nothing here!</Text>
        </View>
      }
      </View>
    );
  }
}
