import React, {
  Component
} from 'react';
import { Platform, StyleSheet, Text, View } from 'react-native';
import { Avatar, Card, ListItem, Button, Icon, Badge } from '../libraries/react-native-elements';

export default class About extends Component{
  render() {
    return (
      <View style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'transparent',
      }}>
        <Avatar
          avatarStyle={{
            backgroundColor: 'transparent'
          }}
          size={'xlarge'}
          rounded
          source={{uri: 'https://images-na.ssl-images-amazon.com/images/I/61HhvJJGBIL._SX425_.jpg'}}
          activeOpacity={0.7}
        />
        <Text style={{
          fontSize: 20,
          textAlign: 'center',
          margin: 10,
        }}>LMAO... Nothing here!</Text>
      </View>
    );
  }
}
